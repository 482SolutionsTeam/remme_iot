// #include <AES.h>
// #include <Arduino.h>
// #include <ed25519.h>
// #include <RemmeApi.h>
// #include <RemmeKeys.h>
// #include <WiFi.h>

// char* host = "192.168.88.242";
// short port = 8000;
// WiFiClient client;

// RemmeApi *remme;
// RemmeKeys *keys;

// const char* ssid = "482.solutions 250 2.4G";
// const char* password = "Qazxsw123";

// void setup() {
//   Serial.begin(115200);
//   WiFi.mode(WIFI_STA);
//   WiFi.begin(ssid, password);
//   Serial.print("\nConnecting to ");
//   Serial.println(ssid);
//   while (WiFi.status() != WL_CONNECTED) {
//     delay(500);
//   }
//   Serial.println(WiFi.localIP());
//   remme = new RemmeApi(host, port);
//   keys = new RemmeKeys(remme);
//   Serial.println("Create and store");
//   keys->create_and_store();
// }

// String getJson(const char* field) {
//     DynamicJsonBuffer jBuffer;
//     JsonObject& response = jBuffer.parseObject(client);
//     response.prettyPrintTo(Serial);
//     Serial.println("");

//     return response[field].as<String>();
// }

// void loop() {
//   Serial.println("Connecting to client");
  
//   if (client.connect("192.168.88.77", 23))
//   {
//     {
//       Serial.println("Send my public key");
//       StaticJsonBuffer<512> jsonBuffer;
//       JsonObject& root = jsonBuffer.createObject();
//       root["public_key"] = keys->byteToHex(keys->public_key, 32);
//       root.prettyPrintTo(client);
//     }

//     while (client.connected() || client.available())
//     {
//       if (client.available())
//       {
//         String key;
//         uint8_t shared_key[32];
//         uint8_t public_k[32];
//         {
//           Serial.println("Get other public key");
//           key = getJson("public_key");
//           keys->hexToByte(key, public_k);

//           Serial.print("Check on blockchain that public key is valid ");
//           Serial.println(keys->check(public_k) ? "True" : "False");
//         }

//         ed25519_key_exchange(
//           shared_key,
//           public_k,
//           keys->private_key
//         );
        
//         Serial.println("Combine my private key with other public key to get shared key:");
//         Serial.println(keys->byteToHex(shared_key, 32));

//         AES256 crypto;
//         uint8_t message[16];
//         uint8_t encryptedMessage[16];
//         crypto.setKey(shared_key, 32);

//         {
//           memset(encryptedMessage, 0, 16);
//           memset(message, 0, 16);
//           memccpy(message, "Hello", NULL, 16);
//           crypto.encryptBlock(encryptedMessage, message);
//           Serial.println("Send message:");
//           Serial.println(String((char*)message));
//           StaticJsonBuffer<512> jsonBuffer;
//           JsonObject& root = jsonBuffer.createObject();
//           root["encryptedMessage"] = keys->byteToHex(encryptedMessage, 16);
//           root.prettyPrintTo(client);
//         }

//         {
//           Serial.println("Get message:");
//           String encryptedMessageS = getJson("encryptedMessage");
//           memset(encryptedMessage, 0, 16);
//           memset(message, 0, 16);
//           keys->hexToByte(encryptedMessageS, encryptedMessage);
//           crypto.decryptBlock(message, encryptedMessage);

//           Serial.print(String((char*)message));
//         }
//       }
//     }
//     client.stop();
//     Serial.println("\n[Disconnected]");
//   } else {
//     Serial.println("Can't connect to client");
//   }
// }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <AES.h>
#include <Arduino.h>
#include <ArduinoJson.h>
#include <ed25519.h>
#include <RemmeApi.h>
#include <RemmeKeys.h>
#include <WiFi.h>

char* host = "192.168.88.242";
short port = 8000;
WiFiClient client;

RemmeApi *remme;
RemmeKeys *keys;


//how many clients should be able to telnet to this ESP8266
#define MAX_SRV_CLIENTS 1
const char* ssid = "482.solutions 250 2.4G";
const char* password = "Qazxsw123";

WiFiServer server(23);

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("I'm server");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  remme = new RemmeApi(host, port);
  keys = new RemmeKeys(remme);
  Serial.println("Create and store");
  keys->create_and_store();

  server.begin();
  server.setNoDelay(true);

  Serial.print("Ready! Use 'telnet ");
  Serial.print(WiFi.localIP());
  Serial.println(":23' to connect");
}

void loop() {
  WiFiClient client = server.available();
  // wait for a client (web browser) to connect
  if (client)
  {
    Serial.println("[Client connected]");
    while (client.connected())
    {
      // read line by line what the client (web browser) is requesting
      if (client.available())
      {
        uint8_t shared_key[32];

        {
          Serial.println("Get other public key");
          DynamicJsonBuffer jsonBuffer;
          JsonObject& response = jsonBuffer.parseObject(client);
          response.prettyPrintTo(Serial);

          uint8_t public_k[32];
          String key = response["public_key"].as<String>();
          keys->hexToByte(key, public_k);
          Serial.print("Check on blockchain that public key is valid ");
          Serial.println(keys->check(public_k) ? "True" : "False");

          ed25519_key_exchange(
            shared_key,
            public_k,
            keys->private_key
          );

          Serial.println("Combine my private key with other public key to get shared key:");
          Serial.println(keys->byteToHex(shared_key, 32));
        }

        {
          Serial.println("Send my public key");
          StaticJsonBuffer<512> jBuffer;
          JsonObject& root = jBuffer.createObject();
          root["public_key"] = keys->byteToHex(keys->public_key, 32);
          root.prettyPrintTo(client);
          Serial.println("");
        }

        AES256 crypto;
        uint8_t message[16];
        uint8_t encryptedMessage[16];
        crypto.setKey(shared_key, 32);

        {
          Serial.println("Get message:");
          DynamicJsonBuffer jBuffer;
          JsonObject& response = jBuffer.parseObject(client);
          response.prettyPrintTo(Serial);
          Serial.println("");

          String encryptedMessageS = response["encryptedMessage"].as<String>();
          memset(encryptedMessage, 0, 16);
          memset(message, 0, 16);
          keys->hexToByte(encryptedMessageS, encryptedMessage);
          crypto.decryptBlock(message, encryptedMessage);

          Serial.println(String((char*)message));
        }

        {
          memset(encryptedMessage, 0, 16);
          memset(message, 0, 16);
          memccpy(message, "Word", NULL, 16);
          crypto.encryptBlock(encryptedMessage, message);

          Serial.println("Send message:");
          Serial.println(String((char*)message));
          StaticJsonBuffer<512> jBuffer;
          JsonObject& root = jBuffer.createObject();
          root["encryptedMessage"] = keys->byteToHex(encryptedMessage, 16);
          root.prettyPrintTo(client);
        }
      }
    }
    // close the connection:
    client.stop();
    Serial.println("[Client disonnected]");
  }
}
