#include <ArduinoJson.h>

class BalanceResponse
{
	private:
	DynamicJsonBuffer buf;
	public:
	unsigned long long int balance;
	String jsonrpc;

	explicit BalanceResponse(String input) : buf (256), balance(25)
	{	
		JsonObject& root = buf.parseObject(input);
		balance = root["result"].as<unsigned long long int>();
		jsonrpc = root["jsonrpc"].as<String>();
	}
	BalanceResponse(const BalanceResponse &base) : buf()
	{
		balance = base.balance;
		jsonrpc = base.jsonrpc;
	}
	~BalanceResponse()
	{

	}
};


