/*
Esp32 library for Remme
*/

#ifndef REMME_API_H
#define REMME_API_H

#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <vector>

using namespace std;
/*
@Brief 
Remme API param - a structure class to hold parameter name and value
*/
class RemmeApiParam
{
	public:
		RemmeApiParam(const String _param = "", String _value = ""): name(_param), value(_value){};
		String getName(){return name;};
		String getValue(){return value;};
	private:
		String name;
		String value;
};

/*
@Brief
Remme API class, used to connect and send messages to a node
*/
class RemmeApi
{	
private:

	char *host;
	short port;
	int timeout;
    WiFiClient client;

	void sendJson(JsonObject &root)
	{
    	if (!this->client.connect(host, port))
		{
			throw "Connection error";
		}
		client.println("POST / HTTP/1.1");
		client.println("Content-Type: application/json");
		client.println("Connection: close");
		client.print("Content-Length: ");
		client.println(root.measurePrettyLength());
		client.println();
		root.prettyPrintTo(client);
		client.println();

		Serial.println("POST / HTTP/1.1");
		Serial.println("Content-Type: application/json");
		Serial.println("Connection: close");
		Serial.print("Content-Length: ");
		Serial.println(root.measurePrettyLength());
		Serial.println();
		root.prettyPrintTo(Serial);
		Serial.println();
	}
	
	JsonObject& getResponse(DynamicJsonBuffer& jsonBuffer)
	{
    	char endOfHeaders[] = "\r\n\r\n";
  		
		if (!client.find(endOfHeaders))
		{
   			Serial.println(F("Invalid response"));
    		throw "Invalid responce exception";
 		}

		JsonObject& response = jsonBuffer.parseObject(client);
		response.prettyPrintTo(Serial);
		Serial.println("");
		Serial.println("");
		return response;
	}

public:	

	/*
	@Brief
	Remme API constructor
	@example
	const char host[] = "node-genesis-testnet-dev.remme.io";
	const short port = 8080;
	RemmeApi remme("node-genesis-testnet-dev.remme.io", 8080);
	*/
	explicit RemmeApi(char *host, short port) : host(host), port(port), timeout(2500) {};

	/*
	@Brief
	Remme Send request funcion
	@params
	String method
	@example
	remme.sendRequest<SimpleResponse>("Example method");
	*/
	template <typename Output>
	Output sendRequest(String method)
	{
		return sendRequest<Output>(method, {});
	};

	/*
	@Brief
	Remme Send request funcion
	@params
	String method
	RemmeApiParam param - parameter and value
	@example
	RemmeApiParam para_01("parameter", "value");
	remme.sendRequest<SimpleResponse>(method, param_01);
	*/
	template <typename Output>
	Output sendRequest(String method, RemmeApiParam param)
	{
		return sendRequest<Output>(method, vector<RemmeApiParam>{ param });
	};
	
	/*
	@Brief
	Remme Send request funcion
	@params
	String method
	vector of RemmeApiParam - standard vector of params
	@example
	vector<RemmeApiParam> vect;
	RemmeApiParam para_01("parameter", "value");
	vect.push_back(para_01);
	remme.sendRequest<SimpleResponse>(method, vect);
	*/
	template <typename Output>
	Output sendRequest(String method, vector<RemmeApiParam> param)
	{
		{
			DynamicJsonBuffer jsonBuffer;
			JsonObject &root = jsonBuffer.createObject();
			root["jsonrpc"] = "2.0";
			root["method"] = method;
			if (param.size() > 0)
			{
				JsonObject &data = root.createNestedObject("params");
				for(int i = 0; i < param.size(); i++)
				{
					data.set(param[i].getName(), param[i].getValue());
				}
			}
			root["id"] = random(1, 100000);
			sendJson(root);
		}

		int a = millis();
		while (!client.available())
		{
			if (millis() - a >= timeout)
			{
				throw "Timeout exception";
			}	
		}
		delay(2);
		String resp;
		{
			DynamicJsonBuffer jsonBuffer(1000);
			getResponse(jsonBuffer).prettyPrintTo(resp);
		}
		return Output(resp);
	}

	/*
	@Brief
	Function for waiting for responce
	@params
	int timeout_ms - value of timeout in milliseconds
	@example
	setTimeout(2500);
	*/
	void setTimeout(int timeout_ms)
	{
		timeout = timeout_ms;
	};
};

#endif