#include <ArduinoJson.h>

class NodeInfoResponse
{
	private:
	DynamicJsonBuffer buf;
	public:
	int peer_count;
	bool is_synced;

	explicit NodeInfoResponse(String input) : buf (256)
	{	
		JsonObject& root = buf.parseObject(input);
		peer_count = root["result"]["peer_count"].as<int>();
		is_synced = root["result"]["is_synced"].as<bool>();
	}
	NodeInfoResponse(const NodeInfoResponse &base) : buf()
	{
		peer_count = base.peer_count;
		is_synced = base.is_synced;
	}
	~NodeInfoResponse()
	{

	}
};


