#ifndef KEYRESPONSE
#define KEYRESPONSE
#include <ArduinoJson.h>

class KeyResponse
{
	private:
	DynamicJsonBuffer buf;
	public:
	uint8_t public_key[32];
	uint8_t private_key[32];

	explicit KeyResponse(String input) : buf (512)
	{	
		JsonObject& root = buf.parseObject(input);
		// root.prettyPrintTo(Serial);
		String tempPubKey = root["result"]["public_key"].as<String>();
		String tempPrivKey = root["result"]["private_key"].as<String>();
		// Serial.println(tempPrivKey);
		// Serial.println(tempPubKey);
		for (int i = 0; i < 32; i++)
		{
			char public_buf[3] = {tempPubKey.charAt(i * 2), tempPubKey.charAt(i * 2 + 1),'\0'};
			//Serial.print(public_buf);
			//Serial.print("-");
			char private_buf[3] = {tempPrivKey.charAt(i * 2), tempPrivKey.charAt(i * 2 + 1), '\0'};
			public_key[i] = (uint8_t) strtoul(public_buf, 0, 16);
			private_key[i] = (uint8_t) strtoul(private_buf, 0, 16);
			// Serial.print(public_key[i]);
			// Serial.print("-");
		}
	}
	KeyResponse(const KeyResponse &base) : buf()
	{
        for(int i = 0; i < 32; i++)
        {
            public_key[i] = base.public_key[i];
            private_key[i] = base.private_key[i];
        }
	}
	~KeyResponse()
	{
        // delete public_key;
        // delete private_key;
	}
};

#endif


