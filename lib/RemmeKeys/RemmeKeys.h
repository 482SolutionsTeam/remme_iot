#ifndef REMME_KEYS_H
#define REMME_KEYS_H

#define HASH_SIZE 64
#define BLOCK_SIZE 128

#include <AnotherEd25519.h>
#include <RemmeApi.h>
#include <KeyResponse.h>
#include <RevokeResp.h>
#include <KeyValidResp.h>
#include <KeyStoredResp.h>
#include <Crypto.h>
#include <Ed25519.h>
#include <RNG.h>
#include <utility/ProgMemUtil.h>
#include <string.h>


class RemmeKeys
{
   private:
    uint8_t seed[32];

   public:
    RemmeApi *api;
    String family_name;
    uint8_t public_key[32];
    uint8_t private_key[64];
    
    String byteToHex(uint8_t *key, int len)
    {
        char buf[192];
        for(int i = 0; i < len; i++)
        {
            char temp_buf[3];
            sprintf(temp_buf,"%02x", key[i]);
            temp_buf[2] = '\0';
            buf[i * 2] = temp_buf[0];
            buf[i * 2  + 1] = temp_buf[1];
        }
        buf[len * 2] = '\0';
        return String(buf);
    }

    void hexToByte(String key, uint8_t* bytes) {
        for (int i = 0; i < key.length(); i++)
        {
          char public_buf[3] = {
            key.charAt(i * 2),
            key.charAt(i * 2 + 1),'\0'
          };
          bytes[i] = (uint8_t) strtoul(public_buf, 0, 16);		
        }
    }

    void computeHash(Hash *hash, uint8_t *hash_val,char *data, uint8_t size)
    {
        size_t posn, len;
        uint8_t value[HASH_SIZE];

        hash->reset();
        for (posn = 0; posn < size; posn += 1) {
            len = size - posn;
            if (len > 1)
                len = 1;
            hash->update(data + posn, len);
        }
        hash->finalize(value, sizeof(value));

        for (int i = 0; i < HASH_SIZE ;i++)
        {
            hash_val[i] = value[i];
        }
    }

    public:
    RemmeKeys(RemmeApi *_api): api(_api), family_name("pub_key")
    {
    }

    void create_and_store()
    {
        //Get key Pair
        ed25519_create_seed(seed);
        ed25519_create_keypair(public_key, private_key, seed);

        vector<RemmeApiParam> vect;
        RemmeApiParam pubKeyPar("public_key", byteToHex(public_key,32));
        vect.push_back(pubKeyPar);
        api->sendRequest<KeyStoredResp>("store", vect);
    }

    bool check(uint8_t* publicKey = NULL)
    {
        if (!publicKey) {
            publicKey = this->public_key;
        }

        RemmeApiParam *par = new RemmeApiParam("public_key", byteToHex(publicKey, 32));
        KeyValidResp _resp = api->sendRequest<KeyValidResp>("check_key", *par);
        return _resp.valid;
    }

    bool revoke(uint8_t* publicKey = NULL)
    {
        if (!publicKey) {
            publicKey = this->public_key;
        }

        RemmeApiParam *par = new RemmeApiParam("public_key", byteToHex(publicKey, 32));
        RevokeResp _resp = api->sendRequest<RevokeResp>("revoke", *par);
        delete par;
        return _resp.revoked;
    }

    String sign(String message)
    {
        char buf[128];
        uint8_t signature_buffer[64];
        int len = message.length();
        message.toCharArray(buf, 64);
        /* create signature on the message with the key pair */
        ed25519_sign(signature_buffer, (const unsigned char *)buf, len, public_key, private_key);
        if(ed25519_verify(signature_buffer, (const unsigned char *)buf, len, public_key) == false)
            Serial.println("ERROR");
        return String(byteToHex(signature_buffer, 65));
        //Create here, return hex string
    }

    bool verify(String signature, String message)
    {
        uint8_t signature_buf[64];
        char message_buf[192];
        int len = message.length();
        message.toCharArray(message_buf, 192);
        for (int i = 0; i < 64; i++)
		{
			char buf[3] = {signature.charAt(i * 2), signature.charAt(i * 2 + 1),'\0'};
			signature_buf[i] = (uint8_t) strtoul(buf, 0, 16);
		}
        return ed25519_verify(signature_buf, (const unsigned char *)message_buf, len, public_key);
    }

    ~RemmeKeys()
    {
        delete public_key;
        delete private_key;
    }

    String generate_adress(char *_family_name, char *data)
    {
        String adress;
        SHA512 sha512;
        uint8_t family_hash[HASH_SIZE];
        computeHash(&sha512, family_hash, _family_name, strlen(_family_name));
        uint8_t data_hash[HASH_SIZE];
        computeHash(&sha512, data_hash, data, strlen(data));

        for(int i = 0; i < 3; i++)
        {
            char buf[3];
            sprintf(buf,"%02x", family_hash[i]);
            buf[2] = '\0';
            adress += buf;
        }

        for(int i = 0; i < 32; i++)
        {
            char buf[3];
            sprintf(buf,"%02x", data_hash[i]);
            buf[2] = '\0';
            adress += buf;
        }
        return adress;
    }

    void setPublicKey(uint8_t *key)
    {
        for (int i = 0; i < 32; i++)
            public_key[i] = key[i];
    }

    void setPrivateKey(uint8_t *key)
    {
        for (int i = 0; i < 32; i++)
            private_key[i] = key[i];
    }
    
    void setPublicKey(String &key)
    {    
        for (int i = 0; i < 32; i++)
		{
			char public_buf[3] = {key.charAt(i * 2), key.charAt(i * 2 + 1),'\0'};
			public_key[i] = (uint8_t) strtoul(public_buf, 0, 16);		
		}
    }
};

#endif