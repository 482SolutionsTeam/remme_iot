#include <ArduinoJson.h>

class KeyStoredResp
{
	private:
	DynamicJsonBuffer buf;
	public:
	bool stored = false;

	explicit KeyStoredResp(String input) : buf (256)
	{	
		JsonObject& root = buf.parseObject(input);
		// root.prettyPrintTo(Serial);
		stored = root["result"]["stored"].as<bool>();
		// Serial.println(valid);
	}

	KeyStoredResp(const KeyStoredResp &base) : buf()
	{
		this->stored = base.stored;
	}

	~KeyStoredResp()
	{
	}
};


