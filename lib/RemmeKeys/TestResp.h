#ifndef TTEST
#define TTEST

#include <ArduinoJson.h>

class TestResp
{
	private:
	DynamicJsonBuffer buf;
	public:


	explicit TestResp(String input) : buf (256)
	{	
		JsonObject& root = buf.parseObject(input);
		root.prettyPrintTo(Serial);
	}
	TestResp(const TestResp &base) : buf()
	{
	}
	~TestResp()
	{
	}
};


#endif