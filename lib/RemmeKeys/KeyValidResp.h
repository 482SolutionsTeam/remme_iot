#include <ArduinoJson.h>

class KeyValidResp
{
	private:
	DynamicJsonBuffer buf;
	public:
	bool valid = false;

	explicit KeyValidResp(String input) : buf (256)
	{	
		JsonObject& root = buf.parseObject(input);
		// root.prettyPrintTo(Serial);
		valid = root["result"]["valid"].as<bool>();
		// Serial.println(valid);
	}
	KeyValidResp(const KeyValidResp &base) : buf()
	{
		this->valid = base.valid;
	}
	~KeyValidResp()
	{
	}
};


