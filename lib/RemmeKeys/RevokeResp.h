#ifndef REVOKERESP
#define REVOKERESP

#include <ArduinoJson.h>

class RevokeResp
{
	private:
	DynamicJsonBuffer buf;
	public:
	bool revoked;


	explicit RevokeResp(String input) : buf (256)
	{	
		JsonObject& root = buf.parseObject(input);
		//root.prettyPrintTo(Serial);
		revoked = root["result"]["revoked"].as<bool>();
		//Serial.print(revoked);
	}
	RevokeResp(const RevokeResp &base) : buf()
	{
		this->revoked = base.revoked;
	}
	~RevokeResp()
	{
	}
};


#endif